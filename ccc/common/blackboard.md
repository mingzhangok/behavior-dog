# 黑板

---

```typescript
// bt.Blackboard.ts
class Blackboard;
```

## API

```typescript
/**
 * 当前黑板有多少个数据
 */
public readonly size: number;

/**
 * 写数据。
 */
public write(key: any, value: any): void;

/**
 * 读数据。
 */
public read(key: any): any;

/**
 * 是否存在数据
 */
public has(key: any): boolean;

/**
 * 擦除一个数据
 */
public delete(key: any): boolean;

/**
 * 擦除所有数据
 */
public clear(): void;

/**
 * 遍历所有数据
 */
public forEach(func: (v: any, k?: any) => void): void;
```