# ConditionTask

---

## Category: Condition/Behavior

### IsBehaviorRunning

行为树是否正在运行。

```typescript
/**
 * 必填项，且排除自己。
 * 待检查的行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;
```

## Category: Condition/Event

### OnEvent

监听标准的行为树事件。

```typescript
/**
 * 必填项。
 * 要监听的事件类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 是否阻止事件继续在行为树中传递。
 * 默认值是 false 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public stopPropagation: boolean;

/**
 * 事件的捕获策略。
 * 默认值是 ANYTIME 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public catchPolicy: EventCatchPolicyType;

/**
 * 事件的过期策略。
 * 默认值是 BEHAVIOR_COMPLETE 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public expiryPolicy: EventExpiryPolicyType;

/**
 * 过期时间。
 * 当过期策略是 PERIOD 时生效。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public expiryTime: number;

/**
 * 是否收到事件。
 */
public readonly received: boolean;

/**
 * 过期事件处理函数。
 * 及时清理掉过时的数据。
 */
protected onEventExipred?(): void;

/**
 * 手动使事件过期。
 */
public expire(): void;
```

支持以下事件捕获策略，

```typescript
enum EventCatchPolicyType {

    /**
     * 随时捕获事件。
     */
    ANYTIME,

    /**
     * 当行为树不运行时，捕获事件。
     */
    BEHAVIOR_IDLE,

    /**
     * 当行为树运行时，捕获事件。
     */
    BEHAVIOR_RUNNING
};
```

支持以下事件过期策略，

```typescript
enum EventExpiryPolicyType {

    /**
     * 保留事件。
     * 需手动调用 @function expire 使事件过期。
     */
    RESERVE,

    /**
     * 事件在指定时间后过期。
     */
    PERIOD,

    /**
     * 事件在任务被评估之后马上过期。
     * 也就是说，事件只在一帧内有效。
     */
    TASK_COMPLETE,

    /**
     * 事件在行为树执行完后过期。
     * 也就是说，不管任务是否被评估，事件在行为树执行结束之前都有效。
     */
    BEHAVIOR_COMPLETE,
};
```

## Category: Condition/System

### IsKeyDown

监听键盘事件。

```typescript
/**
 * 必填项。
 * 按键名（cc.macro.KEY 的键名）。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;
```

## Category: Condition/UI

### OnCCEvent

继承自 ```OnEvent``` 。

1. 即可捕获标准的行为树事件。
2. 也可捕获 ```cc.Node``` 上发射的事件（此事件不会在行为树内传递）。

```typescript
/**
 * 可选项。
 * 发射事件的 cc.Node 节点。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: cc.Node;

/**
 * 事件处理函数。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true ，表示处理此事件，任务评估结果是 SUCCESS 。
 */
protected onCCEvent?(arg1?: any, arg2?: any, arg3?: any): boolean;
```

### OnClick

继承自 ```OnCCEvent``` 。

捕获 ```cc.Button``` 的点击事件。

```typescript
/**
 * @override
 * 如果事件是可冒泡的 cc.Event.bubbles ，是否阻止他冒泡。
 * 默认值是 true 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public stopPropagation: boolean;

/**
 * @override
 * 事件的捕获策略。
 * 默认值是 BEHAVIOR_RUNNING 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public catchPolicy: EventCatchPolicyType;

/**
 * 可选项。
 * 共享变量，储存事件 cc.Event 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public sharedCCEvent: SharedCCEvent;

/**
 * 事件处理函数。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true ，表示处理此事件，任务结果是 SUCCESS 。
 */
protected onClick?(ev: cc.Button | cc.Event, data?: string): boolean;
```

### OnToggle

继承自 ```OnCCEvent``` 。

捕获 ```cc.Toggle``` 的点击事件。

```typescript
/**
 * 可选项。
 * 共享 cc.Toggle 的复选状态 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public checkState: SharedBoolean;

/**
 * 事件处理函数。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true ，表示处理此事件，任务评估结果是 SUCCESS 。
 */
protected onToggle?(toggle: cc.Toggle, data?: string): boolean;
```

### OnCheck

继承自 ```OnToggle``` 。

只捕获 ```cc.Toggle``` 的 ```checked``` 状态。

```typescript
/**
 * 事件处理函数。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true ，表示处理此事件，任务评估结果是 SUCCESS 。
 */
protected onCheck(toggle: cc.Toggle, data?: string): boolean;
```

### OnUnCheck

继承自 ```OnToggle``` 。

只捕获 ```cc.Toggle``` 的 ```unchecked``` 状态。

```typescript
/**
 * 事件处理函数。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true ，表示处理此事件，任务评估结果是 SUCCESS 。
 */
protected onUnCheck(toggle: cc.Toggle, data?: string): boolean;
```

### IsChecked

检查 ```cc.Toggle``` 是否打了勾。

```typescript
/**
 * 必填项。
 * 要检查的 cc.Toggle 组件。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public toggle: cc.Toggle;
```

## Category: Condition/Collision

### OnCollisionEnter

继承自 ```OnEvent``` 。

捕获碰撞检测事件。

参考 [BehaviorTree 碰撞检测](../../behaviortree/custom/component.md#碰撞检测) 。

```typescript
/**
 * 事件处理函数。
 * 返回值可以是 boolean 也可以是 void 。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true 或 void ，任务评估结果是 SUCCESS 。
 */
protected onCollisionEnter?(): boolean | void;
```

### OnCollisionStay

继承自 ```OnEvent``` 。

捕获碰撞检测事件。

参考 [BehaviorTree 碰撞检测](../../behaviortree/custom/component.md#碰撞检测) 。

```typescript
/**
 * 事件处理函数。
 * 返回值可以是 boolean 也可以是 void 。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true 或 void ，任务评估结果是 SUCCESS 。
 */
protected onCollisionStay?(): boolean | void;
```

### OnCollisionExit

继承自 ```OnEvent``` 。

捕获碰撞检测事件。

参考 [BehaviorTree 碰撞检测](../../behaviortree/custom/component.md#碰撞检测) 。

```typescript
/**
 * 事件处理函数。
 * 返回值可以是 boolean 也可以是 void 。
 * 如果返回 false ，表示无视掉此事件，任务评估结果是 FAILURE 。
 * 如果返回 true 或 void ，任务评估结果是 SUCCESS 。
 */
protected onCollisionExit?(): boolean | void;
```

## Category: Condition/Operator

### CmpNum

```
lhs === rhs
lhs !== rhs
lhs  == rhs
lhs  != rhs
lhs  >  rhs
lhs  >= rhs
lhs  <  rhs
lhs  <= rhs
```

比较数字。

```typescript
/**
 * 必填项。
 * 共享变量，储存了表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedNumber;

/**
 * 可选项。
 * 共享变量，储存了表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhs: SharedNumber;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 默认值是 0 。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhsDft: number;

/**
 * 比较操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: NumberCompareOperator;
```

支持以下比较操作符，

```typescript
enum NumberCompareOperator {

    /**
     * 左边的值是否等于右边的值
     */
    '===',

    /**
     * 左边的值是否不等于右边的值
     */
    '!==',

    /**
     * 左边的值是否等于右边的值
     */
    '==',

    /**
     * 左边的值是否不等于右边的值
     */
    '!=',

    /**
     * 左边的值是否大于右边的值
     */
    '>',

    /**
     * 左边的值是否大于或等于右边的值
     */
    '>=',

    /**
     * 左边的值是否小于右边的值
     */
    '<',

    /**
     * 左边的值是否小于或等于右边的值
     */
    '<='
};
```

### CmpBool

```
lhs === rhs
lhs !== rhs
lhs  == rhs
lhs  != rhs
```

比较布尔值。

```typescript
/**
 * 必填项。
 * 共享变量，储存了表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedBoolean;

/**
 * 可选项。
 * 共享变量，储存了表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: SharedBoolean;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 默认值是 true 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhsDft: boolean;

/**
 * 比较操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: BooleanCompareOperator;
```

支持以下比较操作符，

```typescript
enum BooleanCompareOperator {

    /**
     * 左边的值是否等于右边的值
     */
    '===',

    /**
     * 左边的值是否不等于右边的值
     */
    '!==',

    /**
     * 左边的值是否等于右边的值
     */
    '==',

    /**
     * 左边的值是否不等于右边的值
     */
    '!='
};
```

### CmpStr

```
lhs === rhs
lhs !== rhs
lhs  == rhs
lhs  != rhs
```

比较字符串。

```typescript
/**
 * 必填项。
 * 共享变量，储存了表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedString;

/**
 * 可选项。
 * 共享变量，储存了表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: SharedString;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 默认值是 null 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhsDft: string;

/**
 * 比较操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: StringCompareOperator;
```

支持以下比较操作符，

```typescript
enum StringCompareOperator {

    /**
     * 左边的值是否等于右边的值
     */
    '===',

    /**
     * 左边的值是否不等于右边的值
     */
    '!==',

    /**
     * 左边的值是否等于右边的值
     */
    '==',

    /**
     * 左边的值是否不等于右边的值
     */
    '!='
};
```

## Category: Condition/Blackboard

### BbHasKey

黑板上是否存在某个键。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 要检查的键。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

### BbCmpNum

读取黑板上的键值，然后与另一个数字做比较。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 要检查的键。
 * 读取到的值将作为比较表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;

/**
 * 可选项。
 * 共享变量，储存一个数字，作为比较表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: SharedNumber;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 默认值是 0 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhsDft: number;

/**
 * 比较操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: NumberCompareOperator;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

支持以下比较操作符,

```typescript
enum NumberCompareOperator {
    '===',
    '!==',
    '==',
    '!=',
    '>',
    '>=',
    '<',
    '<='
};
```

### BbCmpBool

读取黑板上的键值，然后与另一个布尔值做比较。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 要检查的键。
 * 读取到的值将作为比较表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;

/**
 * 可选项。
 * 共享变量，储存一个布尔值，作为比较表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: SharedBoolean;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 默认值是 true 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhsDft: boolean;

/**
 * 比较操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: BooleanCompareOperator;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

支持以下比较操作符,

```typescript
enum BooleanCompareOperator {
    '===',
    '!==',
    '==',
    '!='
};
```

### BbCmpStr

读取黑板上的键值，然后与另一个字符串做比较。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 要检查的键。
 * 读取到的值将作为比较表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;

/**
 * 可选项。
 * 共享变量，储存一个字符串，作为比较表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: SharedString;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 默认值是 null 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhsDft: string;

/**
 * 比较操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: StringCompareOperator;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

支持以下比较操作符,

```typescript
enum StringCompareOperator {
    '===',
    '!==',
    '==',
    '!='
};
```