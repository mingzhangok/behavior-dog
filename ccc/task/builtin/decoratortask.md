# DecoratorTask

---

## Category: Decorator

### AlwaysSuccess

总是返回成功。

```typescript
// nothing
```

### AlwaysFailure

总是返回失败。

```typescript
// nothing
```

### AlwaysRunning

总是返回运行。

```typescript
// nothing
```

### UntilSuccess

总是返回运行，直到子任务返回成功。

```typescript
// nothing
```

### UntilFailure

总是返回运行，直到子任务返回失败。

```typescript
// nothing
```

### FailureUntilSuccess

总是返回失败，直到子任务返回成功。

```typescript
// nothing
```

### SuccessUntilFailure

总是返回成功，直到子任务返回失败。

```typescript
// nothing
```

### Inverter

对子任务执行结果取反。

```typescript
// nothing
```

### Repeater

重复执行子任务。执行完所有次数后，返回成功，否则返回运行。

- ```0``` 表示不执行子任务，返回失败。
- 负数和 ```Infinity``` 都可表示无限次执行子任务。

```typescript
/**
 * 可选项。
 * 共享变量，储存执行次数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public count: SharedNumber;

/**
 * 当共享变量 count 为空时，使用 countDft 作为执行次数。
 * 默认值是 -1 ，表示无限次执行。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public countDft: number;
```

### FrameRepeater

一帧内重复执行有限次子任务，返回最后一次执行子任务的执行结果。如果子任务返回失败，则中断执行，并返回失败。

- ```0``` 表示不执行子任务，返回失败。
- 不支持无限次执行。

```typescript
/**
 * 可选项。
 * 共享变量，储存执行次数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public count: SharedNumber;

/**
 * 当共享变量 count 为空时，使用 countDft 作为执行次数。
 * 默认值是 1 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public countDft: number;
```

### Period

限定时间内执行子任务，时间到返回成功。

- ```0``` 表示不执行子任务，返回失败。
- 负数和 ```Infinity``` 都可表示无限期执行子任务。

```typescript
/**
 * 可选项。
 * 共享变量，储存执行时间。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public periodTime: SharedNumber;

/**
 * 当共享变量 periodTime 为空时，使用 periodTimeDft 作为执行时间。
 * 默认值是 0 ，表示不执行子任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public periodTimeDft: number;
```

### WithinFrames

指定帧数内执行子任务。

- ```0``` 表示不执行子任务，返回失败。
- 不支持无限帧数。

```typescript
/**
 * 可选项。
 * 共享变量，储存帧数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public frames: SharedNumber;

/**
 * 当共享变量 frames 为空时，使用 framesDft 。
 * 默认值是 1 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public framesDft: number;
```