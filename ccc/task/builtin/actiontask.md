# ActionTask

---

## Category: Action

### Idle

一直运行。

```typescript
// nothing
```

### Wait

等待一段时间。

- ```0``` 表示不等待，直接返回失败。
- 负数和 ```Infinity``` 都可表示无限等待。

```typescript
/**
 * 可选项。
 * 共享变量，储存等待时间。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public waitTime: SharedNumber;

/**
 * 当共享变量 waitTime 为空时，使用 waitTimeDft 。
 * 默认值是 -1 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public waitTimeDft: number;
```

### WaitFrames

等待帧数。

- ```0``` 表示不等待，直接返回失败。
- 负数和 ```Infinity``` 都可表示无限等待。

```typescript
/**
 * 可选项。
 * 共享变量，储存等待帧数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public frames: SharedNumber;

/**
 * 当共享变量 frames 为空时，使用 framesDft 。
 * 默认值是 -1 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public framesDft: number;
```

## Category: Action/Behavior

### SetEnabledBehavior

激活或禁用一个行为树。

```typescript
/**
 * 必填项。
 * 目标行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;

/**
 * 默认值是 TRUE 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: BooleanType;
```

支持以下选项，

```typescript
enum BooleanType {

    /**
     * 激活行为树
     */
    TRUE,

    /**
     * 禁用行为树
     */
    FALSE,

    /**
     * 切换激活或禁用
     */
    TOGGLE
};
```

### SetPausedBehavior

暂停或恢复一个行为树。

```typescript
/**
 * 必填项。
 * 目标行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;

/**
 * 默认值是 TRUE 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: BooleanType;
```

支持以下选项，

```typescript
enum BooleanType {

    /**
     * 暂停行为树
     */
    TRUE,

    /**
     * 恢复行为树
     */
    FALSE,

    /**
     * 切换暂停或恢复
     */
    TOGGLE
};
```

### RestartBehavior

重启一个行为树。

```typescript
/**
 * 必填项。
 * 要执行的行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;
```

### TerminateBehavior

终止执行一个行为树。

```typescript
/**
 * 必填项。
 * 要终止的行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;
```

### TickBehavior

执行 ```1``` 帧外部行为树，并返回其执行结果。

当外部行为树的执行策略 ```tickType``` 是手动执行 ```MANUAL``` 时，生效。

可将该外部行为树作为一个子树看待：

**1)** 当外部行为树被视为一个子树时，其生命周期受 ```TickBehavior``` 任务的生命周期影响。 ```TickBehavior``` 的执行结果代表外部行为树的执行结果。

举个例子，

当 ```TickBehavior``` 的执行结果为 ```RUNNING``` 时，说明目标行为树处于运行状态。

此时如果中断 ```TickBehavior``` 任务，目标行为树也会跟着被打断。

**2)** 当外部行为树不作为一个子树看待时， ```TickBehavior``` 会立即返回成功或失败。

```typescript
/**
 * 必填项，且排除自己。
 * 要执行的行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;

/**
 * 是否将外部行为树视为子树。
 * 默认值是 true 。
 */
public asSubTree: boolean;
```

## Category: Action/Scene

### LoadScene

加载场景。

```typescript
/**
 * 必填项。
 * 要加载的场景名字。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public sceneName: string;
```

### SetActive

激活或禁用节点。

```typescript
/**
 * 必填项。
 * 共享变量，储存一个 cc.Node 节点。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public sharedNode: SharedCCNode;

/**
 * 默认值是 TRUE 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: BooleanType;
```

支持以下选项，

```typescript
enum BooleanType {

    /**
     * 激活节点
     */
    TRUE,

    /**
     * 禁用节点
     */
    FALSE,

    /**
     * 切换激活或禁用
     */
    TOGGLE
};
```

### SetEnabled

激活或禁用组件。

```typescript
/**
 * 必填项。
 * 共享变量，储存一个 cc.Component 节点。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public sharedComp: SharedCCComponent;

/**
 * 默认值是 TRUE 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: BooleanType;
```

支持以下选项，

```typescript
enum BooleanType {

    /**
     * 激活组件
     */
    TRUE,

    /**
     * 禁用组件
     */
    FALSE,

    /**
     * 切换激活或禁用
     */
    TOGGLE
};
```

## Category: Action/Game

### PauseGame

调用 ```cc.director.pause()``` 暂停游戏场景。

```typescript
// nothing
```

### ResumeGame

调用 ```cc.director.resume()``` 暂停游戏场景。

```typescript
// nothing
```

## Category: Action/Collision

### SetEnabledCollider

继承自 ```SetEnabled``` 。

激活或禁用碰撞组件。

```typescript
/**
 * 必填项。
 * 共享变量，储存一个 cc.Collider 组件。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public sharedComp: SharedCCCollider;
```

## Category: Action/Trigger

### CtrlGate

此任务需配合 ```Gate``` 任务使用。

控制 ```Gate``` 任务开关。

```typescript
/**
 * 必填项。
 * 开关任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: Gate;

/**
 * 控制开关。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public ctrl: GateCtrlType;
```

支持以下控制，

```typescript
enum GateCtrlType {

  /**
   * 打开。
   */
  OPEN,

  /**
   * 关闭。
   */
  CLOSE,

  /**
   * 切换开关。
   */
  TOGGLE
};
```

### RequestInterrupt

此任务需配合 ```Interrupt``` 任务使用。

请求 ```Interrupt``` 中断子任务。

```typescript
/**
 * 必填项。
 * 中断任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: Interrupt;
```

## Category: Action/Event

### FireEvent

选取一个任务，向他发射一个事件。

```typescript
/*
 * 必填项。
 * 目标任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: ITask;

/**
 * 必填项。
 * 冒泡事件的类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 可选项。
 * 事件的第 1 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg1: string;

/**
 * 可选项。
 * 事件的第 2 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg2: string;

/**
 * 可选项。
 * 事件的第 3 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg3: string;
```

### FireEvents

选取多个任务，向他们发射同一个事件。

```typescript
/*
 * 必填项。
 * 目标任务列表。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public targets: ITask[];

/**
 * 必填项。
 * 冒泡事件的类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 可选项。
 * 事件的第 1 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg1: string;

/**
 * 可选项。
 * 事件的第 2 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg2: string;

/**
 * 可选项。
 * 事件的第 3 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg3: string;
```

### EmitEvent

选取一个任务，向他发射一个 **向上传递** 的冒泡事件。

```typescript
/**
 * 必填项。
 * 目标任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: ITask;

/**
 * 必填项。
 * 冒泡事件的类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 可选项。
 * 事件的第 1 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg1: string;

/**
 * 可选项。
 * 事件的第 2 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg2: string;

/**
 * 可选项。
 * 事件的第 3 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg3: string;
```

### BroadcastEvent

选取一个分支任务，向他发射一个 **向下传递** 的广播事件。

```typescript
/*
 * 必填项。
 * 目标任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBranchTask;

/**
 * 必填项。
 * 事件的类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 可选项。
 * 事件的第 1 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg1: string;

/**
 * 可选项。
 * 事件的第 2 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg2: string;

/**
 * 可选项。
 * 事件的第 3 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg3: string;
```

### BroadcastEventEx

向一个行为树广播事件。

```typescript
/*
 * 必填项。
 * 目标行为树。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: IBehaviorTree;

/**
 * 必填项。
 * 事件的类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 可选项。
 * 事件的第 1 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg1: string;

/**
 * 可选项。
 * 事件的第 2 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg2: string;

/**
 * 可选项。
 * 事件的第 3 个参数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public arg3: string;
```

### WaitForEvent

等待一个事件。

收到事件返回成功，否则一直运行。

```typescript
/**
 * 必填项。
 * 事件的类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: string;

/**
 * 是否阻止事件继续在行为树中传递。
 * 默认值是 false 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public stopPropagation: boolean;
```

## Category: Action/Operator

### SetNum

```
lhs = rhs
lhs += rhs
lhs -= rhs
lhs *= rhs
lhs /= rhs
```

输入 ```2``` 个数字，计算结果赋给左边的值。

```typescript
/**
 * 必填项。
 * 共享变量，储存了表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedNumber;

/**
 * 可选项。
 * 共享变量，储存了表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhs: SharedNumber;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 表达式右边的值。
 * 默认值是 0 。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhsDft: number;

/**
 * 计算操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: NumberComputeOperator;
```

支持以下计算操作符，

```typescript
enum NumberComputeOperator {

    /**
     * 赋值
     */
    '=',

    /**
     * 加法
     */
    '+=',

    /**
     * 减法
     */
    '-=',

    /**
     * 乘法
     */
    '*=',

    /**
     * 除法
     */
    '/='
};
```

### SetBool

```
lhs = rhs
lhs = !rhs
```

输入 ```2``` 个布尔值，计算结果赋给左边的值。

```typescript
/**
 * 必填项。
 * 共享变量，储存了表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedBoolean;

/**
 * 可选项。
 * 共享变量，储存了表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhs: SharedBoolean;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。此时计算操作符无效。
 * 默认值是 TRUE 。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhsDft: BooleanType;

/**
 * 计算操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: BooleanComputeOperator;
```

支持以下计算操作符，

```typescript
enum BooleanComputeOperator {

    /**
     * 赋值
     */
    '=',

    /**
     * 取反
     */
    '!='
};
```

### SetStr

```
lhs = rhs
```

输入 ```2``` 个字符串，计算结果赋给左边的值。

```typescript
/**
 * 必填项。
 * 共享变量，储存了表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedString;

/**
 * 可选项。
 * 共享变量，储存了表达式右边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhs: SharedString;

/**
 * 当共享变量 rhs 为空时，使用 rhsDft 作为表达式右边的值。
 * 表达式右边的值。
 * 默认值是 null 。
 * @btprop 在 behavior-dog 属性检查器上可见
  */
public rhsDft: string;

/**
 * 计算操作符。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: StringComputeOperator;
```

支持以下计算操作符，

```typescript
enum StringComputeOperator {

    /**
     * 复制字符串
     */
    '='
};
```

## Category: Action/Math

### GenRandom

随机数生成器。

```typescript
/**
 * 必填项。
 * 共享变量，储存表达式左边的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedNumber;

/**
 * 随机数的下边界。
 * 默认值是 0 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public min: number;

/**
 * 随机数的上边界。
 * 默认值是 1 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public max: number;

/**
 * 取整。
 * 默认值是 - ，不转换。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public toInt: FloatToInt;
```

支持以下取整，

```typescript
enum FloatToInt {
    '-',
    floor,
    ceil,
    round
};
```

## Category: Action/Blackboard

### BbDelKey

删除黑板上的一个键。不管黑板上是否存在该键都返回成功。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 要删除的黑板键。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

### BbClear

清除黑板上所有键。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

### BbWrite

写入黑板值。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 黑板键。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;

/**
 * 必填项。
 * 共享变量，储存要写入黑板的值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public rhs: SharedVariable;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```

### BbRead

读取黑板值。

```typescript
/**
 * 黑板类型。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public type: BlackboardType;

/**
 * 必填项。
 * 黑板键。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public key: string;

/**
 * 必填项。
 * 共享变量，储存读取的黑板值。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public lhs: SharedVariable;
```

支持以下黑板类型，

```typescript
enum BlackboardType {

    /**
     * 本地黑板。
     */
    LOCAL_BOARD,

    /**
     * 全局黑板。
     */
    GLOBAL_BOARD
};
```