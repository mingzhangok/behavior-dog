# 内置任务

---

* [ActionTask](actiontask.md)
* [ConditionTask](conditiontask.md)
* [CompositeTask](compositetask.md)
* [DecoratorTask](decoratortask.md)
* [IntercepterTask](interceptertask.md)