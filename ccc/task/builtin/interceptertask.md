# IntercepterTask

---

## Category: Intercepter

### PreCondition

前置条件。

此任务接受一个条件任务 ```ConditionTask``` 作为前置测试条件，测试通过了，才执行子任务，否则返回失败。

可以不提供前置条件。如果不提供，则跳过测试步骤，直接执行子任务。

```typescript
/**
 * 可选项。
 * 前置条件。如果不提供，则跳过测试步骤，直接执行子任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public target: ConditionTask;

/**
 * 通过条件。判断测试是否通过。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public pass: ConditionStatus;
```

支持以下通过条件，

```typescript
enum ConditionStatus {

    /**
     * 条件任务返回成功时，视为测试通过，否则未通过。
     */
    FAILURE,

    /**
     * 条件任务返回失败时，视为测试通过，否则未通过。
     */
    SUCCESS
};
```

### PreConditions

前置条件。

此任务可接受多个条件任务 ```ConditionTask``` 作为前置测试条件，测试通过了，才执行子任务，否则返回失败。

可以不提供前置条件。如果不提供，则跳过测试步骤，直接执行子任务。

```typescript
/**
 * 可选项。
 * 前置条件。如果不提供，则跳过测试步骤，直接执行子任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public targets: ConditionTask[];

/**
 * 通过条件。判断测试是否通过。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public pass: ConditionStatus;

/**
 * 按列表顺序对各个条件任务的执行结果做测试，
 * 然后对测试结果做逻辑操作得出最终测试结果。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public op: BinaryOperator;
```

支持以下通过条件，

```typescript
enum ConditionStatus {

    /**
     * 条件任务返回成功时，视为测试通过，否则未通过。
     */
    FAILURE,

    /**
     * 条件任务返回失败时，视为测试通过，否则未通过。
     */
    SUCCESS
};
```

支持以下二进制操作符，

```typescript
enum BinaryOperator {

    /**
     * a = b && c && d ...
     */
    AND,

    /**
     * a = b || c || d ...
     */
    OR
};
```

### Delay

延迟执行子任务。

- ```0``` 表示不延迟，直接执行子任务。
- 负数和 ```Infinity``` 都可表示无限期延迟执行子任务。

```typescript
/**
 * 可选项。
 * 共享变量，储存延迟时间。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public delayTime: SharedNumber;

/**
 * 当共享变量 delayTime 为空时，使用 delayTimeDft 作为延迟时间。
 * 默认值是 0 ，表示不延迟。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public delayTimeDft: number;
```

### Cooldown

冷却子任务。

执行一次子任务之后，进入冷却状态，在此期间一直返回运行，且不会执行子任务。

冷却时间到才可重新执行子任务。

- ```0``` 表示不冷却，直接执行子任务。
- 负数和 ```Infinity``` 都可表示无限期冷却子任务。

```typescript
/**
 * 可选项。
 * 共享变量，储存冷却时间。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public cdTime: SharedNumber;

/**
 * 当共享变量 cdTime 为空时，使用 cdTimeDft 作为冷却时间。
 * 默认值是 0 ，表示不冷却。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public cdTimeDft: number;

/**
 * 当任务被打断时执行的策略。
 * 默认值是 CLEAR_TIMER 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
pulbic abortPolicy: CooldownAbortPolicyType;
```

支持以下打断策略，

```typescript
enum CooldownAbortPolicyType {

    /**
     * 冷却时间归零。下一帧开始，可以执行子任务。
     */
    CLEAR_TIMER,

    /**
     * 重置冷却时间。下一帧开始，重新开始冷却倒数。
     */
    RESET_TIMER,

    /**
     * 保留当前剩余冷却时间。下一帧开始，继续当前冷却倒数。
     */
    RESERVE_TIMER,
};
```

### AfterFrames

指定帧数后执行子任务。

- ```0``` 表示不执行子任务，直接返回失败。
- 不支持无限帧数。

```typescript
/**
 * 可选项。
 * 共享变量，储存帧数。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public frames: SharedNumber;

/**
 * 当共享变量 frames 为空时，使用 framesDft 。
 * 默认值是 1 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public framesDft: number;
```

### Gate

开关任务。

- 打开时，执行子任务。
- 关闭时，不执行子任务。

开关状态可由以下方法控制，

- 通过触发器 ```CtrlGate``` ，主动改变开关状态。
- 通过事件驱动，被动改变开关状态。

```typescript
/**
 * 开关是否打开。
 * 默认值是 true 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public opened: boolean;

/**
 * 可选项。
 * 事件列表。收到列表中的事件时，打开开关。
 * 默认值是 null 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public eventsOpen: string[];

/**
 * 可选项。
 * 事件列表。收到列表中的事件时，关闭开关。
 * 默认值是 null 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public eventsClose: string[];

/**
 * 可选项。
 * 事件列表。收到列表中的事件时，切换开关。
 * 默认值是 null 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public eventsToggle: string[];

/**
 * 手动打开开关。
 */
public open(): void;

/**
 * 手动关闭开关。
 */
public close(): void;

/**
 * 手动切换开关。
 */
public toggle(): void;
```

### Interrupt

中断任务。

中断可由以下方法触发，

- 通过触发器 ```RequestInterrupt``` ，主动发起中断请求。
- 通过事件驱动，被动中断。

```typescript
/**
 * 可选项。
 * 事件列表。收到列表中的事件时，中断子任务。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public eventsIrq: string[];

/**
 * 手动中断。
 */
public req(): void;
```