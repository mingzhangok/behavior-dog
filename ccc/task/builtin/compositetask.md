# CompositeTask

---

## Category: Composite

### Sequence

按顺序执行子任务。

一个子任务执行成功才会执行下一个，直到全部子任务都执行成功后，返回成功。

否则返回执行不成功的那个子任务的执行结果。

```typescript
// nothing
```

### SequenceTicker

按顺序轮流执行子任务，并且 **一帧只执行一个子任务** 。

```typescript
// nothing
```

### Selector

按顺序执行子任务。

一个子任务执行失败才会执行下一个，直到全部子任务都执行失败后，返回失败。

否则返回执行不失败的那个子任务的执行结果。

```typescript
// nothing
```

### * RevalCompositeTask

在 ```Factory``` 选项卡上不可见，不能直接创建这个任务。

每帧从头开始执行子任务。

继承此任务的有，

- ```RevalSequence```
- ```RevalSelector```

```typescript
// nothing
```

### RevalSequence

继承自 ```RevalCompositeTask``` 。

每帧从头开始，按顺序执行子任务。

一个子任务执行成功才会执行下一个，直到全部子任务都执行成功后，返回成功。

否则返回执行不成功的那个子任务的执行结果，同时打断正在执行的低优先级子任务。

```typescript
// nothing
```

### RevalSelector

继承自 ```RevalCompositeTask``` 。

每帧从头开始，按顺序执行子任务。

一个子任务执行失败才会执行下一个，直到全部子任务都执行失败后，返回失败。

否则返回执行不失败的那个子任务的执行结果，同时打断正在执行的低优先级子任务。

```typescript
// nothing
```

### * RandomCompositeTask

在 ```Factory``` 选项卡上不可见，不能直接创建这个任务。

```RandomCompositeTask``` 主要干了这么一件事：每次任务开始时随机打乱子任务的执行顺序。

继承此任务的有，

- ```RandomSequence```
- ```RandomSelector```

```typescript
/**
 * 生成随机子任务索引列表。
 * @function onEnter 调用了此方法，所以任务每次都会以随机顺序执行子任务。
 */
protected rebuildChildIndies(): void;

/**
 * 子类需在 @function onChildExecuted 里调用此方法。
 * @returns 下一个要执行的子任务索引值。
 */
protected getNextChildIndex(): number;
```

### RandomSequence

继承自 ```RandomCompositeTask``` 。

按随机顺序执行子任务。

一个子任务执行成功才会执行下一个，直到全部子任务都执行成功后，返回成功。

否则返回执行不成功的那个子任务的执行结果。

```typescript
// nothing
```

### RandomSelector

继承自 ```RandomCompositeTask``` 。

按随机顺序执行子任务。

一个子任务执行失败才会执行下一个，直到全部子任务都执行失败后，返回失败。

否则返回执行不失败的那个子任务的执行结果。

```typescript
// nothing
```

### RandomTicker

随机选一个子任务执行，并且 **一帧只执行一个子任务** 。

```typescript
// nothing
```

### * PriorityCompositeTask

在 ```Factory``` 选项卡上不可见，不能直接创建这个任务。

优先级随机任务的基类。

1. 子任务的优先级由其权值 ```weight``` 决定，权值 ```weight``` 越高，被选中的机率越大，若权值 ```weight``` 为 ```0``` ，则永远不会被选中。
2. 子任务可以在运行时通过调整自身的权值 ```weight``` 来影响 ```PriorityCompositeTask``` 随机索引值的生成。

继承此任务的有，

- ```PriorityTicker```

```typescript
/**
 * @returns 随机生成的子任务索引值。
 */
protected getRandomIndex(): number;
```

### PriorityTicker

继承自 ```PriorityCompositeTask```

随机选一个子任务执行，并且 **一帧只执行一个子任务** 。

权重越高，被选中的机率越大。

```typescript
// nothing
```

### * ParallelCompositeTask

在 ```Factory``` 选项卡上不可见，不能直接创建这个任务。

并行任务的基类。

继承此任务的有，

- ```Parallel```
- ```ParallelSelector```
- ```ParallelComplete```

```typescript
// nothing
```

### Parallel

继承自 ```ParallelCompositeTask``` 。

并行执行子任务。有一个子任务返回失败时结束任务并返回失败，否则返回运行。

```typescript
// nothing
```

### ParallelSelector

继承自 ```ParallelCompositeTask``` 。

并行执行子任务。有一个子任务返回成功时结束任务并返回成功，否则返回运行。

```typescript
// nothing
```

### ParallelComplete

继承自 ```ParallelCompositeTask``` 。

并行执行子任务。有一个子任务返回成功或失败时结束任务并返回成功，否则返回运行。

```typescript
// nothing
```