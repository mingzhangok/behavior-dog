# 引用一个任务

---

分 ```4``` 步走，

1. 设置属性的数据类型参数 ```type``` 。
2. 属性检查器上对应的属性将出现一个任务拾取器，双击他，进入 ```pick-up``` 模式。
3. 绘图区将高亮所有可选的任务，模糊化其他不符合要求的任务。从高亮的任务中选一个。
4. 回到属性检查器，点击 ```Done``` 按钮完成拾取操作。

```typescript
// ReferenceExample.ts
import { btclass, ActionTask } from 'bt.ALL';

@btclass('ReferenceExample')
export class ReferenceExample extends ActionTask {
    // ...    
};
```

```typescript
// Example.ts
import { btclass, btprop, ActionTask } from 'bt.ALL';
import { ReferenceExample } from './ReferenceExample';

@btclass('Example')
export class Example extends ActionTask {

    @btprop({ type: ReferenceExample })
    public target: ReferenceExample;
};
```

## 任务拾取器

![图片不见了？！](res/任务拾取器.png)
![图片不见了？！](res/任务拾取器2.png)
![图片不见了？！](res/任务拾取器3.png)

### 引用标志

当一个任务引用了另一个任务时，被引用的这个任务下面会出现一个 ```引用标志``` 和 ```引用计数``` 。鼠标悬念在他上面时，所有引用了他的任务背景色将变成 ```蓝绿色``` 。

> 不过平时这个 ```引用标志``` 是隐藏着的，只有当鼠标悬停在任务上时他才会出现。

> [!Note]
>
> **引用标志 和 引用计数 只表示任务之间的引用关系，不包含行为树组件和共享变量引用任务的情况。**
>
> **如果需要知道是哪个行为树组件的属性或者哪个共享变量实例引用了任务，开发者需自己在属性检查器上查看。**

![图片不见了？！](res/被引用标志.png)

![图片不见了？！](res/被引用标志2.png)

反过来，当我们点击一个任务时，如果这个任务引用了其他任务，那么那些被引用的任务的边框将变成 ```蓝绿色``` 。

![图片不见了？！](res/被引用标志3.png)