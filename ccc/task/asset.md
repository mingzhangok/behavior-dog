# 加载资源

---

在行为树中加载 ```cc.Asset``` 类型的资源。

## 任务脚本中使用 @btprop 声明 cc.Asset 属性 (_v1.1.0_)

```typescript
// Example.ts
import { btclass, btprop, ActionTask } from 'bt.ALL';

@btclass('Example')
export class Example extends ActionTask {

    @btprop({ type: cc.Prefab })
    public prefab: cc.Prefab;
};
```

值得注意的是，这种方式属于动态加载，需要将资源文件放到 ```assets/resources``` 目录下才能加载成功。

## 组件脚本中使用 @property 声明 cc.Asset 属性

如果不想动态加载资源，可以将行为树里需要用到的所有资源，集中在组件脚本里声明，然后在任务脚本中，通过组件获取。

> [!Note]
> 在组件里声明 ```cc.Asset``` 属性请使用 ```@property``` 装饰器。

看下面这个例子。

自定义行为树组件 ```MyBehaviorTree``` ，声明 ```cc.Prefab``` 类型的属性 ```prefab``` ，

```typescript
// MyBehaviorTree.ts
import { btclass, BehaviorTree } from 'bt.ALL';

const { ccclass, property } = cc._decorator;

@ccclass
@btclass('MyBehaviorTree')
export class MyBehaviorTree extends BehaviorTree {

    @property({ type: cc.Prefab })
    public prefab: cc.Prefab;
};
```

然后在 ```CocosCreator``` 的属性检查器里设置，

![图片不见了？！](res/设置资源.png)

设置完资源，就可以在 [任务脚本里获取到他了](custom/task.md#行为树) 。

自定义行动任务 ```MyAction``` ，

```typescript
// MyAction.ts
import { btclass, ActionTask } from 'bt.ALL';
import { MyBehaviorTree } from './MyBehaviorTree';

@btclass('MyAction')
export class MyAction extends ActionTask {

    protected onLoad(): void {

        const tree = this.getBehaviorTree<MyBehaviorTree>();
        const prefab = tree.prefab;

        // ...
    }
}
```