# 自定义 ConditionTask

---

```typescript
// bt.ConditionTask.ts
class ConditionTask extends LeafTask;
```

参考 [父类 LeafTask API](leaftask.md) 。

与其他任务不同，条件任务的执行结果只能是成功或者失败。

## API

```typescript
/**
 * 是否对结果取反。
 * 默认值是 false 。
 */
public invert: boolean;
```

## 举个例子

下面我们创建一个用于检查血量的条件任务。

这个任务比较简单，他有 ```2``` 个属性变量，一个是 [共享变量](../../sharedvariable/index.mh) ```sharedHP``` ，一个是血量阈值 ```threshold``` 。

当他被评估时，会读取 ```sharedHP``` 里储存的血量值，然后与阈值进行比较，低于或等于阈值时返回成功，否则返回失败。

```typescript
import { btclass, btprop, Status, ConditionTask, SharedNumber } from 'bt.ALL';

@btclass('IsLowHp', 'Custom/Condition', {
    description: '检查血量，低于某个值时返回成功。'
})
export class IsLowHP extends ConditionTask {

    @btprop({
        type: SharedNumber,
        required: true,
        tooltip: '血量？'
    })
    public sharedHP: SharedNumber;

    @btprop({
        type: cc.Integer,
        min: 0,
        tooltip: '多少血量算低血量？'
    })
    public threshold: number = 30;

    /**
     * 读取血量值，然后简单做个比较。
     */
    protected onUpdate(): Status {

        return this.sharedHP && this.sharedHP.value <= this.threshold
            ? Status.SUCCESS
            : Status.FAILURE;
    }
};
```