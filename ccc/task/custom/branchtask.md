# BranchTask

---

```typescript
// bt.BranchTask.ts
class BranchTask extends Task;
```

参考 [父类 Task API](task.md) 。

## API

### 子任务

```typescript
/**
 * 子任务数量。
 */
public readonly childCount: number;

/**
 * 当前执行的子任务的索引值。
 */
public readonly childIndex: number;

/**
 * 通过索引值查找子任务。
 */
public at(index: number): ITask;

/**
 * 返回子任务的索引值。
 */
public indexOf(task: ITask): number;

/**
 * 递归遍历子任务。
 */
public walk(func: (task: ITask) => void): void;

/**
 * 遍历子任务。不会递归遍历子任务的子任务。
 */
public forEach(func: (task: ITask, index?: number) => void): void;

/**
 * 测试子任务。不会递归测试子任务的子任务。当有一个子任务通过测试时，返回 true ，否则返回 false 。
 */
public some(func: (task: ITask, index?: number) => boolean): boolean;

/**
 * 测试子任务。不会递归测试子任务的子任务。当所有子任务都通过测试时，返回 true ，否则返回 false 。
 */
public every(func: (task: ITask, index?: number) => boolean): boolean;

/**
 * 返回第一个指定类型的子任务。不会递归查找。
 */
public getChild<T extends ITask>(type: { new (uuid: string): T }): T;

/**
 * 返回所有指定类型的子任务。不会递归查找。
 */
public getChildren<T extends ITask>(type: { new (uuid: string): T }, out?: T[]): T[];

/**
 * 返回第一个通过测试的子任务。不会递归查找。
 */
public getChildBy(func: (task: ITask) => boolean): ITask;

/**
 * 返回所有通过测试的子任务。不会递归查找。
 */
public getChildrenBy(func: (task: ITask) => boolean, out?: ITask[]): ITask[];

/**
 * 递归查找并返回第一个指定类型的任务。
 */
public getTask<T extends ITask>(type: { new (uuid: string): T }): T;

/**
 * 递归查找并返回所有指定类型的任务。
 */
public getTasks<T extends ITask>(type: { new (uuid: string): T }, out?: T[]): T[];

/**
 * 递归查找并返回第一个通过测试的任务。
 */
public getTaskBy(func: (task: ITask) => boolean): ITask;

/**
 * 递归查找并返回所有通过测试的任务。
 */
public getTasksBy(func: (task: ITask) => boolean, out?: ITask[]): ITask[];
```

### 事件系统

在基础事件系统上，添加了 ```broadcast``` 方法，用来向分支上的所有任务广播事件。

```typescript
/**
 * 向一个分支任务发射事件，并向下广播给该分支上的所有任务，直到其中一个任务的 onEvent
 * 方法返回 true 或者没有任务为止。
 * 俗称广播事件。
 * 返回 true 表明事件传递被中止。
 */
public broadcast(type: string, arg1?: any, arg2?: any, arg3?: any): boolean | void;
```