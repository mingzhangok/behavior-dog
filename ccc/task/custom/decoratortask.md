# 自定义 DecoratorTask

---

```typescript
// bt.DecoratorTask.ts
class DecoratorTask extends BranchTask;
```

参考 [父类 BranchTask API](branchtask.md) 。

装饰任务只接受一个子任务，他会首先执行该子任务，然后再根据子任务的执行结果决定自身的状态。

## API

### 装饰子任务

```typescript
/**
 * 装饰函数。
 * 装饰子任务的执行结果。
 * @param status 子任务的执行结果
 * @returns 装饰后的结果
 */
protected onDecorate?(status: Status): Status;

/**
 * 装饰函数的调用时机。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public phase: DecoratorPhaseType;

/**
 * 装饰任务是可叠加的。
 * 多个装饰任务叠加形成一条装饰链。
 * @property taskToDecorate 可以方便地获取到装饰链最终要装饰的那个任务，即装饰链
 * 下面的第一个任务。
 */
public readonly taskToDecorate: ITask;
```

支持以下几种装饰函数的调用时机，

```typescript
enum DecoratorPhaseType {

    /**
     * 不管子任务返回什么结果，都调用装饰函数。
     */
    ALWAYS,

    /**
     * 当子任务返回 SUCCESS 或 FAILURE 时，调用装饰函数。
     */
    COMPLETE,

    /**
     * 当子任务返回 SUCCESS 时，调用装饰函数。
     */
    SUCCESS,

    /**
     * 当子任务返回 FAILURE 时，调用装饰函数。
     */
    FAILURE,

    /**
     * 当子任务返回 RUNNING 时，调用装饰函数。
     */
    RUNNING,

    /**
     * 当子任务不返回 SUCCESS 时，调用装饰函数。
     */
    NOT_SUCCESS,

    /**
     * 当子任务不返回 FAILURE 时，调用装饰函数。
     */
    NOT_FAILURE
};
```