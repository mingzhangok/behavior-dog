# 自定义 IntercepterTask

---

```typescript
// bt.IntercepterTask.ts
class IntercepterTask extends BranchTask;
```

参考 [父类 BranchTask API](branchtask.md) 。

拦截任务与装饰任务的用法一样，都是只接受一个子任务，且支持叠加。

与装饰任务不同的是，拦截任务是在执行子任务之前执行。

使用拦截任务可以帮助我们达到一些效果，

- 控制子任务的执行。
    - 比如做某种测试，测试通过了才执行子任务，不通过则不执行子任务或终止正在执行的子任务。
- 修改子任务的数据。
    - 比如读取黑板值，再赋值给子任务的属性。

## API

### 拦截子任务

```typescript
/**
 * 拦截函数。
 * @returns FAILURE 时表示不拦截，可以正常执行子任务。否则不执行子任务。
 */
protected onIntercept?(): Status;

/**
 * 拦截函数的调用时机。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public phase: IntercepterPhaseType;

/**
 * 可选项。
 * 共享变量，储存拦截函数的调用时间间隔。
 * 当拦截函数的调用时机 phase 是 UPDATE_INTERVAL 时生效。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public phaseInterval?: SharedNumber;

/**
 * 当拦截函数的调用时机 phase 是 UPDATE_INTERVAL 时生效。
 * 当共享变量 phaseInterval 为空时，使用 phaseIntervalDft 作为拦截时间间隔。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public phaseIntervalDft: number;

/**
 * 当拦截函数的调用时机 phase 是 UPDATE 或 UPDATE_INTERVAL 时生效。
 * 当成功拦截子任务（即拦截函数 @function onIntercept 不返回 FAILURE）时，
 *
 *     1. 如果 @function onIntercept 返回 SUCCESS ，则任务的最终执行结果由
 *        属性 statusWhenIntercepted 指定。支持 SUCCESS 和 FAILURE 2 种结果。
 *
 *     2. 如果 @function onIntercept 返回的是 RUNNING ，则任务的执行结果也
 *        将是 RUNNING 。
 *
 * 默认值是 FAILURE 。
 * @btprop 在 behavior-dog 属性检查器上可见
 */
public statusWhenIntercepted: ConditionStatus;

/**
 * 拦截任务是可叠加的。
 * 多个拦截任务叠加形成一条拦截链。
 * 通过属性 taskToIntercept 可以方便地获取到拦截链最终要拦截的那个任务，即
 * 拦截链下面的第一个任务。
 */
public readonly taskToIntercept: ITask;
```

支持以下几种拦截函数的调用时机，

```typescript
enum IntercepterPhaseType {

    /**
     * 在生命周期函数 onEnter 之前，调用拦截函数。即只在任务开始前调用拦截函数。
     * 如果拦截成功，则不会继续调用 onEnter ，任务返回 FAILURE 。
     */
    ENTER,

    /**
     * 在生命周期函数 onUpdate 之前，调用拦截函数。即每帧调用拦截函数。
     * 如果拦截成功，则不会继续调用 onUpdate ，任务可能返回 RUNNING 、 SUCCESS
     * 或 FAILURE 。
     */
    UPDATE,

    /**
     * 在生命周期函数 onUpdate 之前，每隔 phaseInterval 时间调用拦截函数，而非每帧调用。
     * 如果拦截成功，则不会继续调用 onUpdate ，任务可能返回 RUNNING 、 SUCCESS
     * 或 FAILURE 。
     */
    UPDATE_INTERVAL
};
```