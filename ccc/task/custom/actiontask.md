# 自定义 ActionTask

---

```typescript
// bt.ActionTask.ts
class ActionTask extends LeafTask;
```

参考 [父类 LeafTask API](leaftask.md) 。

## 举个例子

下面我们创建一个用于移动 ```cc.Node``` 节点的行动任务。

```typescript
import { btclass, btprop, Status, ActionTask, SharedCCVec2 } from 'bt.ALL';

@btclass('MoveTo', 'Custom/Action', {
    description: '使用 cc.Tween 移动一个节点到目标位置。'
})
export class MoveTo extends ActionTask {

    @btprop({
        type: cc.Node,
        required: true,
        tooltip: '移动哪个节点？'
    })
    public target?: cc.Node;

    @btprop({
        type: SharedCCVec2,
        required: true,
        tooltip: '移动到哪个位置？'
    })
    public sharedPosition?: SharedCCVec2;

    @btprop({
        type: cc.Float,
        min: 0,
        required: true,
        validator: (value: number) => value > 0,
        tooltip: '移动的时间？'
    })
    public duration: number = 1;
};
```

这个任务有 ```3``` 个变量在编辑器上可见。

1. ```target``` 是待操作的 ```cc.Node``` 节点。
2. ```sharedPosition``` 是 [共享变量](../../sharedvariable/index.md) ，储存了一个位置信息，由其他任务提供。
3. ```duration``` 是大于 ```0``` 的浮点数，指定移动时间。 [required](../../common/btprop.md#required) 选项和验证函数 [validator](../../common/btprop.md#validator) 的存在，意味着在行为树编辑阶段，如果用户试图将一个小于或等于 ```0``` 的值赋给 ```duration``` ，编辑器会抛出一个 [错误消息](../../debug/error/task.md) 。

具体的移动操作通过 ```cc.Tween``` 缓动函数实现。他并不需要被每帧调用，所有我们在 ```onEnter``` 阶段就开始执行他，然后在 ```onUpdate``` 处返回 ```RUNNING``` 表示当前正在执行移动操作。当移动完成后，再让 ```onUpdate``` 返回成功 ```SUCCESS``` 。

因此，我们还需要有下面这 ```2``` 个私有变量，

1. 布尔属性 ```_done``` ，表示移动是否完成。通过在 ```cc.Tween``` 完成时注册回调，我们可以更新这个标志，结束当前任务。
2. ```_tween``` 缓存了当前 ```cc.Tween``` 实例，以便在任务中断时停止移动操作。

最后，我们需要在退出任务时重置数据，

1. 重置 ```_done``` 标志。
2. 清除 ```_tween``` 缓存，因为他已经失效了。

**完整的任务脚本如下：**

```typescript
import { btclass, btprop, Status, ActionTask, SharedCCVec2 } from 'bt.ALL';

@btclass('MoveTo', 'Custom/Action', {
    description: '使用 cc.Tween 移动一个节点到目标位置。'
})
export class MoveTo extends ActionTask {

    @btprop({
        type: cc.Node,
        required: true,
        tooltip: '移动哪个节点？'
    })
    public target: cc.Node;

    @btprop({
        type: SharedCCVec2,
        required: true,
        tooltip: '移动到哪个位置？'
    })
    public sharedPosition: SharedCCVec2;

    @btprop({
        type: cc.Float,
        min: 0,
        required: true,
        validator: (value: number) => value > 0,
        tooltip: '移动的时间？'
    })
    public duration: number = 1;

    /**
     * 缓存
     */
    private _tween: cc.Tween;
    private _done: boolean = false;

    /**
     * 缓动函数只需执行一次。
     * 我们把他放到 onEnter 处执行，然后 onUpdate 只检查移动是否完成。
     */
    protected onEnter(): boolean {

        //
        // 检查属性是否正确设置
        //
        if (this.target &&
            this.sharedPosition &&
            this.sharedPosition.vec2 &&
            this.duration > 0) {

            //
            // 开始执行缓动函数
            //
            this._tween = cc.tween(this.target)
                .to(this.duration, {
                    position: this.sharedPosition.vec2
                })
                .call(() => this._done = true)
                .start();

            return true;

        } else {

            //
            // 打印错误信息
            //
            this.log.e('移动失败');

            //
            // 任务将返回结果 Status.FAILURE
            //
            return false;
        }
    }

    /**
     * 移动结束返回成功，否则返回运行。
     */
    protected onUpdate(): Status {

        return this._done ? Status.SUCCESS : Status.RUNNING;
    }

    /**
     * 移动被打断。
     */
    protected onAbort(): void {

        if (this._tween) {
            this._tween.stop();
        }
    }

    /**
     * 退出任务时，清除缓存数据。
     */
    protected onExit(): void {

        this._tween = undefined;
        this._done = false;
    }
};
```