# 自定义 CompositeTask

---

```typescript
// bt.CompositeTask.ts
class CompositeTask extends BranchTask;
```

参考 [父类 BranchTask API](branchtask.md) 。

## API

### 执行子任务

```typescript
/**
 * @returns 第一个被执行的子任务的索引值。默认返回 0 。
 */
protected getInitialChildIndex(): number;

/**
 * 当一个子任务被执行后调用。
 * @param index 子任务的索引值
 * @param status 子任务的执行结果
 * @returns 下一个要被执行的子任务的索引值
 *          如果返回的是无效索引值，则不再继续执行子任务。
 *          如果返回的是传进来的这个索引值，则该子任务会再被执行一次。
 *          也就是说，如果一直返回同一个索引值，将进入死循环。
 */
protected onChildExecuted?(index: number, status: Status): number;
```

### 条件中断

当高优先级的条件任务状态发生变化时，打断正在执行的低优先级任务。

关于条件中断，请参考 [任务打断机制](../abort.md) 章节。

```typescript
/**
 * 设置打断类型。
 * 默认值是 NONE 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public abortType: AbortType;

/**
 * 是否可以打断子任务。
 */
public readonly canAbortSelf: boolean;

/**
 * 是否可以打断低优先级的兄弟任务。
 */
public readonly canAbortLowerPriority: boolean;

/**
 * 设置打断类型。
 */
public setAbortType(type: AbortType): void;

/**
 * 中断触发。
 * @param index 发起中断的子任务的索引值
 */
protected onConditionalAbort?(index: number): void;
```

支持以下中断类型，

```typescript
enum AbortType {

    /**
     * 不打断。
     */
    NONE,

    /**
     * 打断子任务。
     */
    SELF,

    /**
     * 打断低优先级的任务。
     */
    LOWER_PRIORITY,

    /**
     * 打断子任务和低优先级的任务。
     */
    BOTH
};
```