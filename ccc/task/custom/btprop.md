# 属性装饰器 btprop

---

属性装饰器 ```btprop``` 用来注册一个类属性，显示在属性检查器上。

```typescript
// bt.editor.js
function btprop();
function btprop(params: object);
```

## params

```typescript
{
    type?: Function | object | string;
    visible?: boolean;
    tooltip?: string;
    displayName?: string;
    placeholder?: string;
    array?: boolean;
    readonly?: boolean;
    required?: boolean | string;
    validator?: (value: any) => boolean;
    multiline?: boolean;
    inputtype?: string;
    precision?: number;
    min?: number;
    max?: number;
    step?: number;
    slide?: boolean;
    highlighted?: boolean;
    excludeSelf?: boolean;
}
```

### type

属性的数据类型。

支持以下数据类型，

- ```string```
- ```number```
- ```boolean```
- ```bt.BehaviorTree```
- ```bt.Task```
- ```bt.SharedVariable```
- ```cc.Enum```
- ```cc.Integer```
- ```cc.Float```
- ```cc.Node```
- ```cc.Component```
- ```cc.Vec2```
- ```cc.Vec3```
- ```cc.Color```
- ```cc.Asset``` (_v1.1.0_)

### visible

是否在属性检查器上可见。

默认值 true 。

### tooltip

冒泡提示。

### displayName (_v1.2.1_)

显示的属性名。

### placeholder

属性类型是 ```string``` 或 ```number``` 时生效。

### array

是否为数组。可从 ```type``` 自动推断出。

### readonly

只读属性。

### required

```typescript
{
    required: false | true | 'error' | 'warning'
}
```

当属性值无效时，

- 如果 ```required``` 的值是 ```true``` 或 ```error```, 编辑器会抛出一个错误： ```prop "xxx" not set```。
- 如果 ```required``` 的值是 ```warning``` ，编辑器会抛出一个警告： ```prop "xxx" not set``` 。
- 如果 ```required``` 的值是 ```false``` 或 ```undefined``` ，编辑器什么也不做。

> 关于错误和警告的说明，请看 [调试](../../debug/index.md) 章节。
>
> 关于属性值无效的说明，请看 [validator](#validator) 章节。

举个例子，

```typescript
// Example.ts
import { btclass, btprop, ActionTask } from 'bt.ALL';

@btclass('Example')
export class Example extends ActionTask {

    @btprop({ type: cc.Node, required: true })
    public target: cc.Node;
};
```

属性 ```target``` 的数据类型是 ```cc.Node``` ，并且 ```required``` 选项为 ```true``` 。

说明 ```target``` 不能为 ```null``` ，如果 ```null``` ，编辑器将抛出错误： ```prop "target" not set``` 。

![图片不见了？！](../../common/res/属性required.png)

给他赋值才能解决这个错误。

![图片不见了？！](../../common/res/属性required2.png)

### validator

验证函数。

当 ```required``` 选项不为 ```false``` 或 ```undefined``` 时，生效。

属性值发生变化时调用，返回 ```true``` ，代表验证通过，否则不通过，属性值无效。

#### default validator

当 ```validator``` 为 ```undefined``` 时，编辑器将根据以下规则来验证属性值。

- 类型为 ```number``` ```boolean``` ```enum``` 的属性，直接通过。
- 类型为 ```string``` ```object``` 的属性，不为 ```null``` 时通过。

### multiline

多行文本。

属性类型是 ```string``` 时，生效。

### inputtype

输入类型。

属性类型是 ```number``` 时，生效。

```typescript
{
    inputtype: 'float' | 'int';
}
```

> 当数据类型是 ```cc.Float``` 或 ```cc.Integer``` 时，可自动推断。

### precision

输入精度。

属性类型是 ```number``` 时，生效。

### min

限制输入的最小值。

属性类型是 ```number``` 时，生效。

### max

限制输入的最大值。

属性类型是 ```number``` 时，生效。

### step

递增的步长。

属性类型是 ```number``` 时，生效。

### slide

使用 slide 控件。

属性类型是 ```number``` 时，生效。

### highlighted

高亮。

属性类型是 ```cc.Node``` 时，生效。

### excludeSelf

排除自己。

属性类型是 ```BehaviorTree``` 时，生效。

看下图，这是一个 ```BehaviorTree``` 选择器有 ```self``` 选项时的样子，

![图片不见了？！](../../common/res/属性excludeSelf.png)

由于某些原因，你可能想在这个列表中去掉 ```self``` 选项，以避免选择自己。这时候就可以使用这个参数了。

```typescript
import { btclass, btprop, ActionTask, BehaviorTree } from 'bt.ALL';

@btclass('Example', 'MyCustom/MyExample')
export class Example extends ActionTask {
    
    @btprop({ type: BehaviorTree, excludeSelf: true })
    public target: BehaviorTree;
};
```

![图片不见了？！](../../common/res/属性excludeSelf2.png)