# 类装饰器 btclass

---

类装饰器 ```btclass``` 用来注册一个 ```Task``` 。

```typescript
// bt.editor.js
function btclass(className: string);
function btclass(className: string, category: string);
function btclass(className: string, params: object);
function btclass(className: string, category: string, params: object);
```

他接受最多 ```3``` 个参数，其中第一个参数 ```className``` 是必须的，其他参数是可选的。

## className

注册类名。

![图片不见了？！](res/任务类名.png)

## category

任务分组。

```Factory``` 选项卡的任务列表依据这个分组名来对任务进行归类。

支持子分组。与路径名类似，父分组名和子分组名用 ```/``` 字符连接，通过一个字符串来表示层级结构。

```
A/B/C/D
```

比如下面这个 ```Example``` 类，分组名为 ```MyCustom/MyExample```,

```typescript
// Example.ts
import { btclass, ActionTask } from 'bt.ALL';

@btclass('Example', 'MyCustom/MyExample')
export class Example extends ActionTask {
    // ...
};
```

他在 ```Factory``` 选项卡上是显示成这样的，

![图片不见了？！](res/分组.png)

分组名可以为空。

当分组名为空时，将继承父类的分组。比如下面这个 ```ExampleChild``` 类，

```typescript
// ExampleChild.ts
import { btclass } from 'bt.ALL';
import { Example } from 'Example';

@btclass('ExampleChild')
export class ExampleChild extends Example {
    // ...
};
```

他没提供分组名，于是将自动继承父类 ```Example``` 的分组 ```MyCustom/MyExample``` 。

![图片不见了？！](res/分组继承.png)

> **但是如果父类也没有分组，则该任务将不会出现在 Factory 选项卡上，你将不能创建该任务。**

## params

```typescript
{
    visible?: boolean;
    visibleChildren?: boolean;
    description?: string;
    childCountMin?: number;
    childCountMax?: number;
}
```

### visible

是否在 ```Factory``` 选项卡上显示。

默认值 true 。

### visibleChildren

是否自己和子孙都不在 ```Factory``` 选项卡上显示。

如果为 ```false``` ，则子类一律不在 ```Factory``` 选项卡上显示，无论子类是否提供了 ```visible``` 选项。

如果为 ```true``` ，不强制显示子类，子类根据其 ```visible``` 选项而定。

默认值 true 。

## description

对任务的默认描述。

他不是类的一个变量，但在属性检查器上以名为 ```description``` 的属性显示，他的 ```tooltip``` 就是这个默认描述。

> 但因为他不是类的变量，你不能在脚本里直接访问他。

你可以在属性检查器上修改他，以进一步描述任务的用途。

![图片不见了？！](res/描述.png)

## childCountMin

最小子任务数（不小于 ```1``` 的整数）。

只对分支任务有效。

他不是类的一个变量，但在属性检查器上以名为 ```childCountMin``` 的属性显示。

> 但因为他不是类的变量，你不能在脚本里直接访问他。

- 当你提供了这个参数时，属性检查器上的 ```childCountMin``` 属性为 ```readonly``` 属性，意味着你不能修改他。
- 当你不提供这个参数时，属性检查器上的 ```childCountMin``` 属性可被修改。

默认值 ```1``` 。

## childCountMax

最大子任务数（不小于 ```1``` 的整数）。

只对分支任务有效。

他不是类的一个变量，但在属性检查器上以名为 ```childCountMax``` 的属性显示。

> 但因为他不是类的变量，你不能在脚本里直接访问他。

- 当你提供了这个参数时，属性检查器上的 ```childCountMax``` 属性为 ```readonly``` 属性，意味着你不能修改他。
- 当你不提供这个参数时，属性检查器上的 ```childCountMax``` 属性可被修改。

默认值 ```Infinity``` 。
