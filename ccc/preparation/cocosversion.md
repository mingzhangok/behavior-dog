# CocosCreator 版本

---

## 建议 CocosCreator v2.4 以上版本安装

## v2.4

因为 **v2.0 ~ v2.3** 版出现的编辑器崩溃问题，在 **v2.4** 版被修复了。

这是 **v2.4.0** 的更新日志，里面只有这么一条跟插件相关，

![图？](res/更新.png)

## v2.0 ~ v2.3

其实，**v2.0 ~ v2.3** 版本的 ```CocosCreator``` 是可以正常使用插件的。

只不过当插件重新加载时，有时会导致 ```CocosCreator``` 编辑器崩溃。

<!--
比如，

- 插件打开的情况下，直接关闭编辑器， ```3``` 小时后再打开会报错，然后 **崩溃** 。
-->