# 插件脚本

---

由于 **behavior-dog** 自带了运行时框架 **bt.js** 和 **BehaviorTree.js** 这 ```2``` 个脚本文件，

因此，首次安装 **behavior-dog** 时，会弹出下面这个对话框，询问是否将 **bt.js** 导入为 “插件”。

> [!Note]
> **注意** 此处的 “插件” 指的是 [插件脚本](https://docs.cocos.com/creator/manual/zh/scripting/plugin-scripts.html) ，而非 **behavior-dog** 插件。

## 选择 **否** ，不导入为 “插件脚本” 。

![图片不见了？！](res/不设置插件脚本.png)

## 不小心导入为 “插件脚本” ？

如果不小心点了 **是** ，将脚本导入为了 “插件脚本” ，你会在控制台看到下面这条错误：找不到 ```bt``` 模块。

![图片不见了？！](res/插件脚本报错.png)

此时不要慌，只要关闭 ```CocosCreator``` 编辑器再重新打开就好了，因为 **behavior-dog** 会自动将 “插件脚本” 改为 “非插件脚本” 。