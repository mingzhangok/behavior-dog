# 工具栏

---

![图片不见了？！](res/工具栏.png)

1. 窗口置顶按钮（_v1.1.0_）
2. 翻页按钮
    - 绑定键盘/鼠标事件 ```pageup``` 和 ```pagedown``` （_v1.3.0_）
3. 将当前编辑页面设为启动页（_v1.3.0_）
4. [预览按钮](../debug/preview.md)
5. 停止预览按钮
6. 行为树选择器
7. 菜单

## 菜单

![图片不见了？！](res/菜单.png)

| <div style="width:170px">菜单项</div> | 说明 |
| --- | --- |
| Close | 退出当前行为树，回到欢迎界面。 |
| Show Task Index | 在绘图区任务卡上显示该任务在父任务上的索引值。父任务根据该索引值依次执行子任务。 编辑器通过任务卡的 ```x``` 坐标值来维护索引值，最左边的任务索引值为 ```0```, 其右边第一个兄弟任务索引值为 ```1```, 依此类推。此选项并不会影响索引值的生成。 |
| Show cc.Node Path | 在绘图区左下角显示当前行为树所在 ```cc.Node``` 节点在节点树中的路径。看 [这里](../behaviortree/overview.md#其他) 。 |
| Show Warnings | 在绘图区任务卡上显示黄色的警告标签。当然，如果任务本身没有任何警告信息，什么也不会显示。警告不同于错误 ，行为树在有警告的情况下也是可以照常运行的。此选项只是用于调试，如果不想看到这个黄标，可以把他关掉。 |
| Blur In-Active Tasks | 模糊化没有连接到行为树的任务。 |
| Enable Editor Log | 在 ```CocosCreator``` 控制台里打印更多调试信息。 |
| Dump | 在 ```CocosCreator``` 控制台里打印当前行为树的序列化数据。 |
| Time Capsule | 呼叫出 [时间胶囊](../timecapsule/index.md) 面板。 |
| *** Delete Behavior Data *** | 删除当前行为树上的所有 [任务](../task/index.md) 和 [共享变量](../sharedvariable/index.md)，回到初始状态。 |