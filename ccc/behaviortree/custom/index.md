# 自定义行为树组件

---

* [类装饰器 btclass](btclass.md)
* [属性装饰器 btprop](btprop.md)
* [行为树组件 BehaviorTree](component.md)
    * [日志](component.md#日志)
    * [状态](component.md#状态)
    * [控制](component.md#控制)
    * [Task](component.md#task)
    * [数据共享](component.md#数据共享)
    * [事件系统](component.md#事件系统)
    * [生命周期](component.md#生命周期)
    * [CocosCreator 专有 API](component.md#cocoscreator-专有-api)
        * [资源文件](component.md#资源文件)
        * [CocosCreator2D](component.md#cocoscreator2d)
            * [UI 事件](component.md#ui-事件)
            * [碰撞检测](component.md#碰撞检测)