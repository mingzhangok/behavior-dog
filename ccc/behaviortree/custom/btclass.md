# 类装饰器 btclass

---

类装饰器 ```btclass``` 用来注册一个行为树。

```typescript
// bt.editor.js
function btclass(className: string);
```

## className

注册类名。

通过 ```btclass``` 注册的行为树组件类名将会显示在属性检查器的标题上。

![图片不见了？！](res/组件名.png)