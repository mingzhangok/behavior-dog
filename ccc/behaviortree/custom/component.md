# BehaviorTree

---

```typescript
// bt.BehaviorTree.ts
class BehaviorTree extends cc.Component;
```

## API

```typescript
/**
 * 描述行为树。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 * @property 该属性在 CocosCreator 的属性检查器上可见
 */
public description: string;

/**
 * 行为树的名字。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 * @property 该属性在 CocosCreator 的属性检查器上可见
 */
public behaviorName: string;
```

### 日志

推荐使用内置的日志工具打印日志。他会自动为消息添加前缀标签，方便调试。

> 此处的日志开关与行为树 ```Task``` 里的日志开关是相互独立的。组件的日志开关，主要是在行为树加载阶段打印调试信息。

```typescript
/**
 * 通过 log.v/d/i/w/e(...args) 可以在控制台输出格式为
 *
 *   [ behavior-dog ] [ 行为树名字<行为树UUID> ] ...args
 *
 * 的日志。
 */
public readonly log: ILogger;

/**
 * 日志开关只会影响到 log.v 。
 * 当日志关闭时， log.v 不会输出日志，但 log.d log.i log.w 和 log.e 能照常输出。
 * 
 * 通过 log.VERBOSE 的值可以判断日志开关是否打开。
 * 
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public enableLog: boolean;
```

### 当前状态

```typescript
/**
 * 行为树执行结果。
 */
public readonly behaviorStatus: Status;

/**
 * 行为树是否正在执行。
 */
public readonly behaviorRunning: boolean;

/**
 * 行为树是否执行完成。
 */
public readonly behaviorCompleted: boolean;

/**
 * 与上一次 tick 的时间间隔。
 */
public readonly dt: number;
```

### 控制

```typescript
/**
 * 激活行为树。
 * 默认值是 true 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public behaviorEnabled: boolean;

/**
 * 暂停行为树。
 * 默认值是 false 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public behaviorPaused: boolean;

/**
 * 当行为树被激活时执行行为树。
 * 默认值是 true 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public startWhenEnable: boolean;

/**
 * 当行为树被禁用时暂停行为树。
 * 默认值是 false 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public pauseWhenDisable: boolean;

/**
 * 当行为树结束时重新执行行为树。
 * 默认值是 false 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public restartWhenComplete: boolean;

/**
 * 行为树执行的 3 种方式。
 *    1. EVERY_FRAME     每帧执行
 *    2. FIXED_SECONDS   固定时间间隔执行
 *    3. MANUAL          手动执行
 * 默认值是 EVERY_FRAME 。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public tickType: TickType;

/**
 * 当行为树的执行方式为 TickType.FIXED_SECONDS 时生效。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public tickSeconds: number;

/**
 * 手动执行行为树。
 * 返回执行结果。
 */
public tick(): Status;

/**
 * 手动终止行为树。
 */
public terminate(): void;
```

### 任务节点

```typescript
/**
 * 遍历所有任务。
 */
public walk(func: (task: ITask) => void): void;

/**
 * 返回第一个指定类型的任务。
 */
public getTask<T extends ITask>(type: { new (uuid: string): T }): T;

/**
 * 返回所有指定类型的任务。
 */
public getTasks<T extends ITask>(type: { new (uuid: string): T }, out?: T[]): T[];

/**
 * 返回第一个通过测试的任务。
 */
public getTaskBy(func: (task: ITask) => boolean): ITask;

/**
 * 返回所有通过测试的任务。
 */
public getTasksBy(func: (task: ITask) => boolean, out?: ITask[]): ITask[];
```

### 数据共享

```typescript
/**
 * 本地黑板。一块行为树内部使用的黑板，可在任务之间传递数据。
 */
public readonly blackboard: IBlackboard;

/**
 * 全局黑板。一块全局共享的黑板，可在场景之间传递数据。
 */
public readonly globalboard: IBlackboard;
```

关于黑板的接口，请看 [这里](../../common/blackboard.md) 。

> 推荐使用另一种数据共享的方式 [SharedVariable](../../sharedvariable/index.md) 。
>
> 但是 ```SharedVariable``` 只支持 ```tree``` 和 ```scene``` ```2``` 个作用域。也就是说，他只能最多做到在同场景中的行为树之间共享数据。
>
> 如果要在场景之间传递数据，只能使用全局黑板 ```globalboard``` 。

### 事件系统

```typescript
/**
 * 向行为树广播事件。
 * 所有任务都可收到。
 * 支持最多3个参数。
 * 详情请参考任务事件系统。
 */
public broadcast(type: string, arg1?: any, arg2?: any, arg3?: any): void;
```

### 生命周期

自定义组件可以通过实现下列函数来监听生命周期回调。

```typescript
/**
 * 行为树加载成功时调用。
 * @param tree
 */
public onBehaviorLoaded?(tree: IBehaviorTree): void;

/**
 * 行为树加载失败时调用。
 * @param tree
 * @param err
 */
public onBehaviorLoadError?(tree: IBehaviorTree, err: string): void;

/**
 * 行为树销毁时调用。
 * @param tree
 */
public onBehaviorDestroy?(tree: IBehaviorTree): void;

/**
 * 行为树激活时调用。
 * @param tree
 */
public onBehaviorEnable?(tree: IBehaviorTree): void;

/**
 * 行为树禁用时调用。
 * @param tree
 */
public onBehaviorDisable?(tree: IBehaviorTree): void;

/**
 * 行为树暂停时调用。
 * @param tree
 */
public onBehaviorPause?(tree: IBehaviorTree): void;

/**
 * 行为树恢复执行时调用。
 * @param tree
 */
public onBehaviorResume?(tree: IBehaviorTree): void;

/**
 * 行为树第一次执行前调用。
 * @param tree
 */
public onBehaviorReady?(tree: IBehaviorTree): void;

/**
 * 行为树开始执行时调用。
 * @param tree
 */
public onBehaviorStart?(tree: IBehaviorTree): void;

/**
 * 行为树重新开始执行时调用。
 * @param tree
 */
public onBehaviorRestart?(tree: IBehaviorTree): void;

/**
 * 行为树执行结束时调用。
 * @param tree
 */
public onBehaviorComplete?(tree: IBehaviorTree): void;
```

### CocosCreator 专有 API

CocosCreator 专有 API 是指组件中使用了 CocosCreator API 的属性或方法。

#### 资源文件

```typescript
/**
 * 行为树的 JSON 资源。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 * @property 该属性在 CocosCreator 的属性检查器上可见
 */
public asset: cc.JsonAsset;

/**
 * 支持动态加载行为树资源。
 * 如果已有资源被加载，调用此方法会报错。
 * @param asset
 */
public loadAsset(asset: cc.JsonAsset): void;
```

#### CocosCreator2D

##### UI 事件

```typescript
/**
 * 监听 cc.Button 的点击事件。
 * 使用方法参考 [官方文档](https://docs.cocos.com/creator/manual/zh/components/button.html#button-点击事件)
 *
 * 点击事件被触发后，组件会向全体行为树任务广播事件 "click" 。
 * 可以使用条件任务 OnClick 进行监听。
 * 
 * 在给 cc.Button 添加回调时，可以传入一个 string 类型的 CustomEventData 。
 * 如果设置了 CustomEventData ，将会做为 onClick 回调的第二个参数传入。
 * @param event
 * @param data
 */
public onClick(event: cc.Event, data?: string): void;

/**
 * 监听 cc.Toggle 的点击事件。
 * 使用方法参考 [官方文档](https://docs.cocos.com/creator/manual/zh/components/toggle.html#toggle-事件)
 *
 * 点击事件被触发后，组件会向全体行为树任务广播事件 "toggle" 。
 * 可以使用条件任务 OnToggle 进行监听。
 * 
 * 在给 cc.Toggle 添加回调时，可以传入一个 string 类型的 CustomEventData 。
 * 如果设置了 CustomEventData ，将会做为 onToggle 回调的第二个参数传入。
 * @param toggle
 * @param data
 */
public onToggle(toggle: cc.Toggle, data?: string): void;
```

下面通过一个例子来说明 UI 事件的用法，

假设场景中有一个按钮节点，选中他，我们可以在 ```CocosCreator``` 的属性检查器上看到挂载的 ```cc.Button``` 组件。

将 ```onClick``` 函数添加到 ```cc.Button``` 组件的点击事件回调列表里，

![图片不见了？！](res/添加按钮事件.png)

这样，当按钮被点击时，行为树就可以响应他了。

> [!Note]
> **注意** 使用这种方法捕获的 ```UI``` 事件会广播给所有任务。
>
> 如果只需要某个任务单独响应 ```UI``` 事件，可以参考内置任务
> [OnCCEvent](../../task/built/conditiontask.md#onccevent) 、
> [OnClick](../../task/built/conditiontask.md#onclick) 、
> [OnToggle](../../task/built/conditiontask.md#ontoggle) 。

##### 碰撞检测

```typescript
/**
 * 可选项。
 * 共享变量，储存一个 cc.Collider 碰撞器。
 * 由碰撞检测回调函数写入。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public sharedColliderOther: SharedCCCollider;

/**
 * 可选项。
 * 共享变量，储存一个 cc.Collider 碰撞器。
 * 由碰撞检测回调函数写入。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public sharedColliderSelf: SharedCCCollider;

/**
 * 启用或取消 "onCollisionEnter" 事件的监听。
 * 启用时，组件内部会为 onCollisionEnter 这个方法赋值，他只做两件事：
 *     1. 把 onCollisionEnter 传入来的两个参数 { other： cc.Collider } 和
 *        { self: cc.Collider } 分别存放到共享变量 sharedColliderOther 和 
 *        sharedColliderSelf 。
 *     2. 然后广播事件 "collision-enter" 。
 *        行为树中的任务可以捕获这个事件，比如使用内置条件任务 OnCollisionEnter 。
 * 取消时，组件内部会把 onCollisionEnter 这个方法设为 undefined.
 *     1. 这样行为树中的任务也就监听不到 "collision-enter" 事件了。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public collisionEnter: boolean;

/**
 * 启用或取消 "onCollisionStay" 事件的监听。
 * 启用时，组件内部会为 onCollisionStay 这个方法赋值，他只做两件事：
 *     1. 把 onCollisionEnter 传入来的两个参数 { other： cc.Collider } 和
 *        { self: cc.Collider } 分别存放到共享变量 sharedColliderOther 和 
 *        sharedColliderSelf 。
 *     2. 然后广播事件 "collision-stay" 。
 *        行为树中的任务可以捕获这个事件，比如使用内置条件任务 OnCollisionStay 。
 * 取消时，组件内部会把 onCollisionStay 这个方法设为 undefined.
 *     1. 这样行为树中的任务也就监听不到 "collision-stay" 事件了。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 */
public collisionStay: boolean;

/**
 * 启用或取消 "onCollisionExit" 事件的监听。
 * 启用时，组件内部会为 onCollisionExit 这个方法赋值，他只做两件事：
 *     1. 把 onCollisionEnter 传入来的两个参数 { other： cc.Collider } 和
 *        { self: cc.Collider } 分别存放到共享变量 sharedColliderOther 和 
 *        sharedColliderSelf 。
 *     2. 然后广播事件 "collision-exit" 。
 *        行为树中的任务可以捕获这个事件，比如使用内置条件任务 OnCollisionExit 。
 * 取消时，组件内部会把 onCollisionExit 这个方法设为 undefined.
 *     1. 这样行为树中的任务也就监听不到 "collision-exit" 事件了。
 * @btprop 该属性在 behavior-dog 的属性检查器上可见
 * */
public collisionExit: boolean;

/**
 * 碰撞事件的回调，由 behavior-dog 实现。
 * 如果你想自己实现这个方法，请参考属性 collisionEnter 的说明。
 */
public onCollisionEnter?(other: cc.Collider, self: cc.Collider): void;

/**
 * 碰撞事件的回调，由 behavior-dog 实现。
 * 如果你想自己实现这个方法，请参考属性 collisionStay 的说明。
 */
public onCollisionStay?(other: cc.Collider, self: cc.Collider): void;

/**
 * 碰撞事件的回调，由 behavior-dog 实现。
 * 如果你想自己实现这个方法，请参考属性 collisionExit 的说明。
 */
public onCollisionExit?(other: cc.Collider, self: cc.Collider): void;
```

下面介绍如何开启组件的碰撞检测回调，

![图片不见了？！](res/碰撞检测.png)

> [!Note]
> **注意** 碰撞检测事件会广播给所有任务。
>
> 目前版本不支持任务单独捕获这个事件。