# 行为树组件概览

---

## 创建行为树

### 属性检查器中创建

#### 1. 添加 BehaviorTree 组件到 cc.Node 节点上。

![图片不见了？！](res/添加组件.png)

#### 2. 指定行为树资源文件。

> [!NOTE]
> 目前支持的文件格式是 ```JSON``` 。

创建一个空的行为树，只需一个合法的空 ```JSON``` 文件：

```json
{}
```

![图片不见了？！](res/添加资源文件.png)

> [!WARNING]
> **注意**，当前版本不支持多个行为树引用同一个 ```JSON``` 资源。
> 编辑器会无视掉引用了同一个 ```JSON``` 资源的行为树。

> 如需复制一个行为树，可以通过下面 ```2``` 种方式实现。

> 1. 先复制行为树资源文件，然后再将副本指定为另一个行为树的资源文件，实现行为树的复制。
>
> 2. 先用一个空的 ```JSON``` 文件创建一个空的行为树，然后再使用 [时间胶囊](../timecapsule/index.md) 复制其他行为树。

#### 3. Ctrl-S 保存场景，刷新行为树列表。

![图片不见了？！](res/行为树列表.png)

#### 4. 点击行为树名字就可以进入编辑界面了。

> [!NOTE]
> 编辑器不支持 ```UNDO``` ```REDO``` 操作。

### 脚本中动态创建

通过在 ```cc.Node``` 节点上动态添加 ```BehaviorTree``` 组件，并调用组件的 ```loadAsset``` 方法可动态创建行为树。

举个例子，

```typescript
// SomeCustomComponent.ts
import { BehaviorTree } from 'bt.ALL';

const { ccclass, property } = cc._decorator;

@ccclass
export class SomeCustomComponent extends cc.Component {

    @property({ type: cc.JsonAsset })
    public asset: cc.JsonAsset;

    protected onLoad(): void {

        //
        // 向 cc.Node 节点动态添加 BehaviorTree 组件，并加载 asset 资源。
        //
        this.node.addComponent(BehaviorTree).loadAsset(this.asset);
    }
};
```

> [!Note]
>
> 关于行为树加载完成的回调，请参考行为树生命周期回调函数 [onBehaviorLoaded](custom/component.md#生命周期) 。

## 销毁行为树

从 ```cc.Node``` 节点上删除 ```BehaviorTree``` 组件即可。

## 属性检查器

顺利创建行为树进入编辑界面之后，你就可以在 ```Behavior``` 选项卡上查看和修改行为树组件的 [属性](custom/component.md) 了。

![图片不见了？！](res/行为树组件属性检查器.png)

### 菜单

![图片不见了？！](res/行为树组件属性检查器菜单.png)

- 点击 ```Reset``` 选项可重置组件属性值。

## 其他

### cc.Node 节点路径

绘图区左下角会显示当前行为树组件所在的 ```cc.Node``` 节点路径。

> 这是一个可选项。如果不想显示这个节点路径，可以勾掉```behavior-dog``` 工具栏菜单的 ```Show cc.Node Path``` 选项。

![图片不见了？！](res/组件在节点树中的路径.png)

### cc.Component.enabledInHierarchy 属性

当行为树组件在节点树中被禁用时，绘图区左下角会显示 ```黄色``` 的 ```disabledInHierarchy``` 字样。

![图片不见了？！](res/组件在节点树中失活.png)

### cc.Component.enabled 属性

当行为树组件被禁用时，绘图区左下角会显示 ```黄色``` 的 ```disabled``` 字样。

![图片不见了？！](res/组件失活.png)