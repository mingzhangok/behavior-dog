# 引用一个行为树组件

---

分 ```2``` 步走，

1. 设置属性的数据类型参数 ```type``` 。
2. 属性检查器上对应的属性处将出现一个行为树选择器，从中选一个即可。

```typescript
import { btclass, btprop, BehaviorTree, ActionTask } from 'bt.ALL';

@btclass('Example')
export class Example extends ActionTask {
    
    @btprop({ type: BehaviorTree })
    public target: BehaviorTree;
};
```

## 行为树选择器

行为树选择器的下拉列表里显示的是

- 所有可选行为树实例的名字（即属性 ```behaviorName``` 的值）。

哪些行为树会出现在选择器里？

> ```type``` 定义的行为树类型及其子类的实例都会出现在行为树选择器的列表里。

> [!Note]
> **引用自己？**
>
> 当列表里出现一个名为 ```self``` 的行为树时，表明这个行为树是当前行为树。
>
> 你可以通过设置属性参数 [excludeSelf](../btprop.md#excludeself) 来把他从列表里剔除。

### 引用标志

当一个任务通过行为树选择器引用了外部行为树时，该任务的右上角将出现一个外部引用标志。

![图片不见了？！](res/外部引用标志.png)