# 引用一个共享变量

---

分 ```2``` 步走，

1. 设置属性的数据类型参数 ```type``` 。
2. 属性检查器上对应的属性将出现一个共享变量选择器，从中选一个即可。

```typescript
import { btclass, btprop, ActionTask, SharedCCNode } from 'bt.ALL';

@btclass('Example')
export class Example extends ActionTask {
    
    @btprop({ type: SharedCCNode })
    public target: SharedCCNode;
};
```

![图片不见了？！](res/共享变量选择器.png)

## 共享变量选择器

共享变量选择器的下拉列表里显示的是，

- 以作用域 ```scope``` 分组的，
- 所有可选共享变量实例的名字（即属性 ```name``` 的值）。

哪些共享变量会出现在列表里？

> ```type``` 定义的共享变量类型及其子类的实例都会出现在选择器的列表里。

### 属性检查器

共享变量选择器自带属性检查器展开按钮。

当引用了一个共享变量后，可以通过这个快捷属性检查器查看和修改他的属性。

如果没有引用共享变量，点击此按钮将没有任何效果。

![图片不见了？！](res/共享变量属性检查器.png)

### 引用计数

除了属性检查器外，共享变量选择器还自带了引用计数。

![图片不见了？！](res/共享变量引用计数.png)