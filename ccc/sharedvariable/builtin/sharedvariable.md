# SharedVariable

---

## SharedString

```typescript
/**
 * 储存一个 string 。
 * 默认值是 undefined 。
 */
public value?: string;
```

## SharedStringList

```typescript
/**
 * 储存一个 string 数组。
 * 默认值是 undefined 。
 */
public value?: string[];
```

## SharedBoolean

```typescript
/**
 * 储存一个 boolean 。
 * 默认值是 true 。
 */
public value: boolean;
```

## SharedBooleanList

```typescript
/**
 * 储存一个 boolean 数组。
 * 默认值是 undefined 。
 */
public value?: boolean[];
```

## SharedNumber

```typescript
/**
 * 储存一个 number 。
 * 默认值是 0 。
 */
public value: number;
```

## SharedNumberList

```typescript
/**
 * 储存一个 number 数组。
 * 默认值是 undefined 。
 */
public value?: number[];
```

## SharedCCNode

```typescript
/**
 * 储存一个 cc.Node 。
 * 默认值是 undefined 。
 */
public value?: cc.Node;
```

## SharedCCNodeList

```typescript
/**
 * 储存一个 cc.Node 数组。
 * 默认值是 undefined 。
 */
public value?: cc.Node[];
```

## SharedCCComponent

```typescript
/**
 * 储存一个 cc.Component 。
 * 默认值是 undefined 。
 */
public value?: cc.Component;
```

## SharedCCComponentList

```typescript
/**
 * 储存一个 cc.Component 数组。
 * 默认值是 undefined 。
 */
public value?: cc.Component[];
```

## SharedCCVec2

```typescript
/**
 * 储存一个 cc.Vec2 。
 * 默认值是 undefined 。
 */
public value?: cc.Vec2;
```

## SharedCCVec2List

```typescript
/**
 * 储存一个 cc.Vec2 数组。
 * 默认值是 undefined 。
 */
public value?: cc.Vec2[];
```

## SharedCCVec3

```typescript
/**
 * 储存一个 cc.Vec3 。
 * 默认值是 undefined 。
 */
public value?: cc.Vec3;
```

## SharedCCVec3List

```typescript
/**
 * 储存一个 cc.Vec3 数组。
 * 默认值是 undefined 。
 */
public value?: cc.Vec3[];
```

## SharedCCColor

```typescript
/**
 * 储存一个 cc.Color 。
 * 默认值是 undefined 。
 */
public value?: cc.Color;
```

## SharedCCColorList

```typescript
/**
 * 储存一个 cc.Color 数组。
 * 默认值是 undefined 。
 */
public value?: cc.Color[];
```

## SharedCCCollider

继承自 ```SharedCCComponent``` 。

```typescript
/**
 * 储存一个 cc.Collider 。
 * 默认值是 undefined 。
 */
public value?: cc.Collider;
```

## SharedCCColliderList

继承自 ```SharedCCComponentList``` 。

```typescript
/**
 * 储存一个 cc.Collider 数组。
 * 默认值是 undefined 。
 */
public value?: cc.Collider[];
```

## SharedCCEvent

```typescript
/**
 * 储存一个 cc.Event 。
 * 默认值是 undefined 。
 */
public value?: cc.Event;
```