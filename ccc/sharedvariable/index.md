# 共享变量

---

共享变量是行为树实现数据共享的一种方法。

> 另一种共享数据的方法是使用 [黑板](../common/blackboard.md) 。

* [概览](overview.md)
* [自定义](custom/index.md)
* [引用](reference.md)
* [内置](builtin/index.md)