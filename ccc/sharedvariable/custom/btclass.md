# 类装饰器 btclass

---

类装饰器 ```btclass``` 用来注册一个 ```SharedVariable``` 。

```typescript
// bt.editor.js
function btclass(className: string);
```

## className

注册类名。

![图片不见了？！](../res/共享选项卡类名.png)