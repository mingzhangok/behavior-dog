# SharedVariable

---

```typescript
// bt.SharedVariable.ts
class SharedVariable;
```

## API

```typescript
/**
 * 全局唯一 UUID 。主要用于编辑器。
 */
public readonly uuid: string;

/**
 * 共享变量的名字。
 */
public name: string;

/**
 * 数据。
 */
public value?: any;
```

## 日志

推荐使用内置的日志工具打印日志。他会自动为消息添加前缀标签，方便调试。

```typescript
/**
 * 通过 log.v/d/i/w/e(...args) 向控制台打印格式为
 *
 *     [ behavior-dog ] [ 行为树名字<行为树UUID>.共享变量类名<共享变量名字> ] ...args
 *
 * 的日志。
 */
public readonly log: ILogger;
```

## 重置

```typescript
/**
 * 重置数据。
 */
public reset(): void;
```

## 举个例子

```typescript
import { btclass, SharedVariable } from 'bt.ALL';

@btclass('SharedEnemyInfo')
export class SharedEnemyInfo extends SharedVariable {

    /**
     * enemy position
     */
    public position?: cc.Vec2;

    /**
     * enemy hp
     */
    public hp: number = 100;
}
```

如果需要在编辑器里设置初始值，可以使用 ```@btprop``` 装饰属性，使其在编辑器里可见。

比如，

```typescript
import { btclass, btprop, SharedVariable } from 'bt.ALL';

@btclass('SharedEnemyInfo')
export class SharedEnemyInfo extends SharedVariable {

    @btprop({
        type: cc.Vec2,
        tooltip: 'enemy position'
    })
    public position?: cc.Vec2;

    @btprop({
        type: cc.Integer,
        min: 0,
        max: 100,
        step: 10,
        tooltip: 'enemy hp'
    })
    public hp: number = 100;
}
```