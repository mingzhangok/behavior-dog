# 行为树任务

---

当任务抛出 ```错误``` 时，

1. 绘图区任务卡的边框会变成 ```红色``` 。
2. 绘图区任务卡的右下角会显示 ```红色``` 的错误标签和错误个数。
3. 任务的属性检查器上显示详细的错误信息。

当任务抛出 ```警告``` 时，

1. 绘图区任务卡的右下角会显示 ```黄色``` 的警告标签和警告个数。
2. 任务的属性检查器上显示详细的警告信息。

## 举个例子

如下图， ```Entry``` 和 ```Sequence``` 这两个任务都报了错误，个数都为 ```1``` 。

![图片不见了？！](res/连接错误.png)

当 ```Entry``` 和 ```Sequence``` 连接后， ```Entry``` 的边框变成了白色，错误标签也消失了，表明所有错误已被解决。但此时 ```Sequence``` 仍有 ```1``` 个错误。

![图片不见了？！](res/连接错误2.png)

![图片不见了？！](res/连接错误3.png)

## 什么情况下会抛出错误？

- 由绘图区引起的连接错误 ```connection error``` 。
- 由任务脚本中使用 ```@btprop``` 装饰的属性引起。
    - ```@btprop``` 设置了 ```required``` 选项, 且值为 ```true``` 或者 ```"error"``` 时，
        - 任务将一直抛出错误 ```prop "XXX" not set``` ，直到该属性被正确赋值为止。
    - 属性类型为 ```BehaviorTree``` ```Task``` ```SharedVariable``` 中任一种时，
        - 如果找不到定义的类型，抛出这个错误 ```prop "XXX" missing type```。
        - 如果找不到定义的值，抛出这个错误 ```prop "XXX" missing reference```。
        - 如果引用的值本身有错误，则抛出这个错误 ```prop "XXX" not executable```。
- ~~由任务脚本中定义的 ```preExecErrorMsg(): string | void``` 方法的返回值引起。~~
    - ~~参考 [自定义 Task 编辑器相关](../task/custom/task.md#编辑器相关)~~

### 关于连接错误

```connection error``` 是由绘图区抛出的泛指连接上的错误。

以下是抛出 ```connection error``` 的条件：

- 对于分支任务， ```childCountMin``` 和 ```childCountMax``` 这两个属性所起到的限制子任务个数的作用，就体现在 ```connection error``` 上。
    - 当子任务个数小于 ```childCountMin``` 或大于 ```childCountMax``` 时，抛出 ```connection error``` 。
    - 当没有父任务时，抛出 ```connection error``` 。
- 对于叶子任务，由于其没有子任务，是否抛出 ```connection error``` 就看他是否有父任务了。
- 对于入口任务 ```Entry```, 只接收一个子任务，如果没有，则抛出 ```connection error``` 。

## 什么情况下会抛出警告？

- 由任务脚本中使用 ```@btprop``` 装饰的属性引起。
    - ```@btprop``` 设置了 ```required``` 选项，且值为 ```warning``` 时，
        - 任务将一直抛出警告 ```prop "XXX" not set``` ， 直到该属性被正确赋值为止。