# 共享变量

---

关于共享变量错误反馈的更多信息，请参考 [引用计数](../../sharedvariable/overview.md#引用计数) 章节。

## 什么情况下会抛出错误？

- 由共享变量脚本中使用 ```@btprop``` 装饰的属性引起。
    - ```@btprop``` 设置了 ```required``` 选项, 且值为 ```true``` 或者 ```error``` 时，
        - 共享变量将一直报错误 ```prop "XXX" not set``` ，直到该属性被正确赋值为止。
    - 属性类型为 ```BehaviorTree``` ```Task``` ```SharedVariable``` 中任一种时，
        - 如果找不到定义的类型，抛出这个错误 ```prop "XXX" missing type``` 。
        - 如果找不到定义的值，抛出这个错误 ```prop "XXX" missing reference``` 。
        - 如果引用的值本身有错误，则抛出这个错误 ```prop "XXX" not executable``` 。

## 什么情况下会抛出警告？

- 由共享变量脚本中使用 ```@btprop``` 装饰的属性引起。
    - ```@btprop``` 设置了 ```required``` 选项，且值为 ```warning``` 时，
        - 共享变量将一直抛出警告 ```prop "XXX" not set``` ，直到该属性被正确赋值为止。

例如，当属性 ```name``` 值为空时，抛出这个警告 ```prop "name" not set``` 。

![图片不见了？！](res/共享选项卡展开警告.png)