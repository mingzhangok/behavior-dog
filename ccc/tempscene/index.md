# ~~临时场景~~ (_TO BE REMOVED_)

---

```CocosCreator``` 会为场景和组件分配一个唯一的永久性 ```UUID```, 用于序列化和反序列化。

```behavior-dog``` 则通过该 ```UUID``` 来管理行为树。

基于这个原因，在下列 ```2``` 个场景中， ```behavior-dog``` 将暂停编辑行为树的操作：

1. 当新建一个场景时， ```CocosCreator``` 为其分配的是一个临时的 ```UUID``` ，此时 ```behavior-dog``` 不可编辑，任何对场景的修改都不会触发 ```behavior-dog``` 更新。
    - 开发者需将临时场景保存为 ```*.fire``` 文件，并双击打开他， ```CocosCreator``` 才会为其生成一个永久性的 ```UUID``` 。
    - 场景保存完成后， ```behavior-dog``` 自动恢复编辑模式。

2. 当打开一个 ```prefab``` 文件时， ```CocosCreator``` 自身将进入 ```prefab``` 编辑模式，此时为该场景分配的也是一个临时 ```UUID``` ，故 ```behavior-dog``` 不可编辑，任何对 ```prefab``` 的修改都不会触发 ```behavior-dog``` 更新。
    - 切换回普通场景时， ```behavior-dog``` 自动恢复编辑模式。

![图片不见了？！](res/临时场景.png)